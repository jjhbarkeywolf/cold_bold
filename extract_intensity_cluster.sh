#/bin/bash
module load fsl/5.0.8-shark

#Dit script werkt op de outputs van bold_intensity.sh
#zoekt voor de outputs van bold_intensity in de working directory
#denk aan omgekeerde indexwaarden van 'cluster'. De hoogste index heeft namelijk de meeste voxels.

#NB: voor elk contrast opnieuw runnen en handmatig de outputs opschonen!!!!!!

#voorbeeld: bash ./extract_intensity.sh "/data/DIV2/radiologie-hpc/JurriaanBarkeyWolf/COLD_BOLD/RUNS/water_0_graden/stats/randomise_outputs/PrePost_4D_5k_ghettomask_tfce_corrp_tstat1.nii.gz" 0.99 /data/DIV2/radiologie-hpc/JurriaanBarkeyWolf/COLD_BOLD/RUNS/water_0_graden/

#geef volume van het contrast waar je de clusters uit wilt halen (absolute filename svp)
contrast=$1

#geef de significantiethreshold (0.99 voor p<0.01 of 0.95 voor p<0.05 etc)
threshold=$2

#geef dir met outputs van bold_intensity.sh
wd=$3

#geef kenmerk van bold intensity outputs per proefpersoon, bijvoorbeeld Cold
kenmerk=Cold

basedir=`pwd`

#####################################################
cd ${wd}
mkdir -p intensity_extraction

#extraheren clusters uit contrast, outputten in file op index volgorde groot -> klein
cluster --in=${contrast} --thresh=${threshold} --mm --oindex=./intensity_extraction/clusters_${threshold}.nii.gz

#uitzoeken hoeveel clusters er zijn
n_clusters=`fslstats ./intensity_extraction/clusters_${threshold}.nii.gz -R | awk '{print $2}'`

#uit elkaar trekken clusters in aparte files
mkdir -p intensity_extraction/clusters_${threshold}
for index in `seq ${n_clusters}`
do
	fslmaths ./intensity_extraction/clusters_${threshold}.nii.gz -uthr $index -thr $index ./intensity_extraction/clusters_${threshold}/cluster_${index}
done

#eerst even alle cluster indices op een rijtje zetten en aangeven om welk contrast het gaat
#let op wat hier gebeurd mbt newlines e.d.
echo $contrast > ./intensity_extraction/values_${threshold}.csv
echo "clusters thresholded at ${threshold}" >> ./intensity_extraction/values_${threshold}.csv
echo >> ./intensity_extraction/values_${threshold}.csv
echo -n "subject" >> ./intensity_extraction/values_${threshold}.csv

for cluster_mask in `ls ./intensity_extraction/clusters_${threshold}`
do
	cluster_nr=`basename ${cluster_mask} .nii.gz`
	echo -n "," ${cluster_nr} >> ./intensity_extraction/values_${threshold}.csv
done
echo >> ./intensity_extraction/values_${threshold}.csv

#extraheren waarden per cluster
for input in `ls -d ${kenmerk}*` 
do
	echo "running ${input}"
	echo -n ${input} >> ./intensity_extraction/values_${threshold}.csv
	for cluster_mask in `ls ./intensity_extraction/clusters_${threshold}`
	do
		value=`fslstats ${input}/*smoothed*_final.nii.gz -k ./intensity_extraction/clusters_${threshold}/${cluster_mask} -m`
		echo -n "," ${value} >> ./intensity_extraction/values_${threshold}.csv
	done
	echo >> ./intensity_extraction/values_${threshold}.csv
done

cat ./intensity_extraction/values_${threshold}.csv

cluster --in=${contrast} --thresh=${threshold} --mm --oindex=./intensity_extraction/clusters_${threshold}.nii.gz > ./intensity_extraction/clusters_information_${threshold}.txt


echo "run done."

cd ${basedir}