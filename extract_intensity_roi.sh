#/bin/bash
module load fsl/5.0.9-shark

#extraheert intensities van een gespecificeerde arbitrary ROI

#Voorbeeld:
#bash ./extract_intensity_roi.sh $roi $outputs
#bash ./extract_intensity_roi.sh poepROI.nii.gz /data/DIV2/radiologie-hpc/JurriaanBarkeyWolf/COLD_BOLD/RUNS/water_0_graden/

#$roi: geef niifile van ROI mask
#$outputs: geef locatie outputs bold_intensity.sh (n.b.: / op het einde)


roi=$1
outputs=$2

ROIname=`basename ${roi} .nii.gz`
mkdir -p ${ROIname}_intensityvalues

basedir=`pwd`
cd ${ROIname}_intensityvalues

echo "subject" "," "intensity" > ${ROIname}_intensities.csv

#extraheren waarden
for input in `ls -d ${outputs}*/*_final.nii.gz` 
do
	basename=`basename $input .nii.gz`

	echo "extracting value of ${ROIname} for ${basename}..."
	echo ${basename} "," `fslstats ${input} -k ${roi} -m`>> ${ROIname}_intensities.csv

done