#!/bin/bash
#$ -S /bin/bash
#$ -q all.q
#$ -pe BWA 1
#$ -N boldIntensity
#$ -cwd
#$ -j Y
#$ -V
#$ -m be

#Ventricle template zelf toevoegen in dezelfde map als dit script en 'lateral_ventricle_mask.nii' noemen



module load fsl/5.0.8-shark

#wil je cleanup? 1 is ja, anders dan 1 is nee.
cleanup=1

#ventrikel template (in working directory zetten!)
vents=lat_ventricles_thr60.nii.gz


#definieren herkenpunten specifieke scans
BOLDscanID=*_drink_*.nii
T1scanID=*3DT1*

#CSF threshold in procenten/100%
CSF_thresh=0.99

#aantal volumes je er aan het begin af wilt knippen. Index begint met 0. '9' trimt dus eerste 10 volumes)
tr_min=9

#Grootte smoothing kernel van eindresultaat (FWHM in mm!!!, dus niet sigma)
FWHM=5


#######################proefpersoon specifieke CODE, itereerbaar op $pp, $source en $prepost######################
sourcedir=$1
pp=$2
prepost=$3


input=${sourcedir}/${pp}*${prepost}*_drink_*.nii
input_T1=${sourcedir}/${pp}*3DT1*.nii
input_hires=${sourcedir}/${pp}*_hires_*.nii

#uitrekenen juiste sigma voor smoothing. Zie http://mathworld.wolfram.com/GaussianFunction.html voor uitleg.
sfactor=2.3548
sigma=$(echo "scale=5;$FWHM/$sfactor" | bc)

##################BOLDSCAN BEWERKINGEN
filename=`basename ${input} .nii`
basedir=`pwd`

#maken en binnentreden working directory voor deze proefpersoon
mkdir -p $filename
cd $filename

#weggooien eerste n volumes d.m.v. fslroi
echo 'dumping first' ${tr_min} '(+1) volumes....'
fslroi ${input} ${filename}_trimmed ${tr_min} -1

#motion correctie met mcflirt, maken bijbehorende grafiekjes #-spline_final wordt ook gebruikt in Feat...
echo 'starting motion correction'
mcflirt -in ${filename}_trimmed -out ${filename}_mcf -mats -plots -spline_final
fsl_tsplot -i ${filename}_mcf.par -t 'MCFLIRT estimated rotations (radians)' -u 1 --start=1 --finish=3 -a x,y,z -w 640 -h 144 -o MC_rotations.png 
fsl_tsplot -i ${filename}_mcf.par -t 'MCFLIRT estimated translations (mm)' -u 1 --start=4 --finish=6 -a x,y,z -w 640 -h 144 -o MC_translations.png 
#fsl_tsplot -i ${filename}_mcf_abs.rms,${filename}_mcf_rel.rms -t 'MCFLIRT estimated mean displacement (mm)' -u 1 -w 640 -h 144 -a absolute,relative -o disp.png

#middelen over de dynamics
echo 'averaging over all dynamics...'
fslmaths ${filename}_mcf -Tmean ${filename}_Tmean

#brain extraction met bet op standaard settings
echo 'starting brain extraction....'
bet ${filename}_Tmean ${filename}_Tmean_bet

echo 'done'

##########################REGISTRATIE
#uitrekenen registratiematrices/registreren naar structural
echo 'Creating subdirectory and simplifying filenames for registration....'
mkdir -p reg
fslmaths ${filename}_Tmean_bet ./reg/example_func
cd reg

#betten T1 en fmri hires
filename_T1=`basename ${input_T1} .nii`
filename_hires=`basename ${input_hires} .nii`
bet ${input_T1} highres
bet ${input_hires} initial_highres

#versimpelen filenames (maakt rommel)
cp ${input_T1} highres_head.nii
fslmaths ${FSLDIR}/data/standard/MNI152_T1_2mm_brain standard
fslmaths ${FSLDIR}/data/standard/MNI152_T1_2mm standard_head
fslmaths ${FSLDIR}/data/standard/MNI152_T1_2mm_brain_mask_dil standard_mask

#co-registratie magie
echo 'starting nonlinear co-registration magic.....'
flirt -in example_func -ref initial_highres -out example_func2initial_highres -omat example_func2initial_highres.mat -cost corratio -dof 6 -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -interp trilinear 
convert_xfm -inverse -omat initial_highres2example_func.mat example_func2initial_highres.mat
slicer example_func2initial_highres initial_highres -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png example_func2initial_highres1.png ; slicer initial_highres example_func2initial_highres -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png example_func2initial_highres2.png ; pngappend example_func2initial_highres1.png - example_func2initial_highres2.png example_func2initial_highres.png; /bin/rm -f sl?.png example_func2initial_highres2.png
echo 'epi_reg output:'
echo '----------'
epi_reg --epi=initial_highres --t1=highres_head --t1brain=highres --out=initial_highres2highres
echo '----------'
echo 'epi reg done'
echo 'starting rest of registration....'
convert_xfm -inverse -omat highres2initial_highres.mat initial_highres2highres.mat
slicer initial_highres2highres highres -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png initial_highres2highres1.png ; slicer highres initial_highres2highres -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png initial_highres2highres2.png ; pngappend initial_highres2highres1.png - initial_highres2highres2.png initial_highres2highres.png; /bin/rm -f sl?.png initial_highres2highres2.png
flirt -in highres -ref standard -out highres2standard -omat highres2standard.mat -cost corratio -dof 12 -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -interp trilinear 
fnirt --iout=highres2standard_head --in=highres_head --aff=highres2standard.mat --cout=highres2standard_warp --iout=highres2standard --jout=highres2highres_jac --config=T1_2_MNI152_2mm --ref=standard_head --refmask=standard_mask --warpres=10,10,10
applywarp -i highres -r standard -o highres2standard -w highres2standard_warp
convert_xfm -inverse -omat standard2highres.mat highres2standard.mat
slicer highres2standard standard -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png highres2standard1.png ; slicer standard highres2standard -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png highres2standard2.png ; pngappend highres2standard1.png - highres2standard2.png highres2standard.png; /bin/rm -f sl?.png highres2standard2.png
convert_xfm -omat example_func2highres.mat -concat initial_highres2highres.mat example_func2initial_highres.mat
flirt -ref highres -in example_func -out example_func2highres -applyxfm -init example_func2highres.mat -interp trilinear
convert_xfm -inverse -omat highres2example_func.mat example_func2highres.mat
slicer example_func2highres highres -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png example_func2highres1.png ; slicer highres example_func2highres -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png example_func2highres2.png ; pngappend example_func2highres1.png - example_func2highres2.png example_func2highres.png; /bin/rm -f sl?.png example_func2highres2.png
convert_xfm -omat example_func2standard.mat -concat highres2standard.mat example_func2highres.mat
convertwarp --ref=standard --premat=example_func2highres.mat --warp1=highres2standard_warp --out=example_func2standard_warp
applywarp --ref=standard --in=example_func --out=example_func2standard --warp=example_func2standard_warp
convert_xfm -inverse -omat standard2example_func.mat example_func2standard.mat
slicer example_func2standard standard -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png example_func2standard1.png ; slicer standard example_func2standard -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png example_func2standard2.png ; pngappend example_func2standard1.png - example_func2standard2.png example_func2standard.png; /bin/rm -f sl?.png example_func2standard2.png
echo 'registration step done'

cd ..

##########################SEGMENTATIE CSF
#extraheren van deze tussenstappen uit reg map voor gebruik in CSF normalisatie.
fslmaths ./reg/example_func2standard ${filename}_Tmean_reg2std
fslmaths ./reg/example_func2highres ${filename}_Tmean_reg2struc

#uitsegmenteren CSF(PVE0) en GM (PVE1)
echo 'starting segmentation of CSF...'
fast ./reg/highres.nii.gz

#CSF-masker thresholden naar x% zekerheid
echo 'thresholding CSF on' ${CSF_thresh}
fslmaths ./reg/highres_pve_0.nii.gz -thr ${CSF_thresh} -bin CSF_thresholded

#terugwarpen van standard space ventricle mask naar subject structural space
echo 'warping supplied standard space ventricle template back to subject structural space...'
flirt -in ../${vents} -ref ./reg/highres.nii.gz -init ./reg/standard2highres.mat -applyxfm -out lateral_ventricle_mask_reg2struc

echo 'masking subjects thresholded CSF mask with structural spaced ventricle template...'
fslmaths CSF_thresholded -mas lateral_ventricle_mask_reg2struc CSF_lateral_ventricle_mask_reg2struc

echo 'done making CSF mask of lateral ventricles, outputting diagnostic pictures (VENT.png)...'
slicer CSF_lateral_ventricle_mask_reg2struc ${filename}_Tmean_reg2struc -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png VENTR1.png ; /bin/rm -f sl?.png
slicer ${filename}_Tmean_reg2struc CSF_lateral_ventricle_mask_reg2struc -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png VENTR2.png ; /bin/rm -f sl?.png

#GM vast sorteren, nog niet thresholden
fslmaths ./reg/highres_pve_1.nii.gz GM_individual_probmap

#############################NORMALISATIESTAP
echo 'calculating CSF intensity....'
#uitrekenen CSF intensity
CSF_intensity=`fslstats ${filename}_Tmean_reg2struc -k CSF_lateral_ventricle_mask_reg2struc -m`

echo 'CSF mean intensity is' ${CSF_intensity}
echo 'starting normalisation by dividing standard space registered BOLDscan with this factor...'
#normaliseren BOLD data in elk voxel dmv delen door CSF_intensity
fslmaths ${filename}_Tmean_reg2std -div ${CSF_intensity} ${filename}_Tmean_reg2std_CSFnorm

echo 'smoothing with a gaussian kernel of FWHM' ${FWHM} 'mm... sigma = '${sigma} 'mm'
#smoothing met vooraf gespecificeerde kernel
fslmaths ${filename}_Tmean_reg2std_CSFnorm -s ${sigma} ${filename}_smoothed${FWHM}mm_final

echo 'done.'


#############################OPRUIMEN
cd ${basedir}
if [ cleanup == 1 ]
	then
	echo 'cleaning up.....'
	#./cleanup.sh `pwd` ${filename}
	rm ${dir}/${filename}_mcf.nii.gz
	rm ${dir}/${filename}_Tmean.nii.gz
	rm ${dir}/${filename}_Tmean_bet.nii.gz
	rm ${dir}/${filename}_trimmed.nii.gz
	#rm ${dir}/${filename}_Tmean_reg2struc
	rm -r ${dir}/reg	
	echo 'cleanup done!'
fi



echo 'BOLD INTENSITY CALCULATION DONE for ' ${filename}