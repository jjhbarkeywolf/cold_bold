#!/bin/bash




###########################NB: DIT IS GEEN SCRIPT, SLECHTS EEN VERZAMLING COMMANDOS#####
mkdir -p stats


ls -d Cold*pre*/*final* > prepost.txt
ls -d Cold*post*/*final* >> prepost.txt

cat prepost.txt

fslmerge -t ./stats/PrePost_4D `cat prepost.txt`

cd stats

#fslview PrePost_4D.nii.gz &


randomise_parallel -i PrePost_4D.nii.gz -o ./randomise_outputs/PrePost_4D_5k_ghettomask -d design.mat -t design.con -e design.grp -m brain_mask.nii.gz -T
