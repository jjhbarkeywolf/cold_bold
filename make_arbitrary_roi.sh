#/bin/bash
module load fsl/5.0.8-shark

#Dit script werkt op de outputs van bold_intensity.sh (slikt dus alleen standard space mni 2mm)
#syntax#:
#bash ./extract_intensity_arbitrary_roi.sh $x $y &z $size $name $mirror

#voorbeeld: `bash ./extract_intensity_arbitrary_roi.sh 12 18 23 8 PoepROI` 


#geef voxel coordinates van gewenste ROI (standard space MNI 2mm)
x=$1
y=$2
z=$3

#geef dir met outputs van bold_intensity.sh ()

#geef gewenste grootte van ROI in mm
size=$4

#geef de gewenste ROI een naam zodat je hem terug kan vinden (geen spaties)
name=$5

#indien 'ja', maakt ROI op zelfde plek in andere hemisphere. Hier moet je iets invullen. Elke andere input resulteert in een ROI in maar 1 hemisphere. (elke andere input )
mirror=$6

####################################################################
basedir=`pwd`
mkdir -p ROI_${name}

cd ROI_${name}

#roi maken
echo "making ROI"
fslmaths ${FSLDIR}/data/standard/MNI152_T1_2mm -mul 0 -add 1 -roi $x 1 $y 1 $z 1 0 1 ${name}_point -odt float
fslmaths ${name}_point -kernel sphere 3 -fmean -bin ${name}_sphere -odt float

# op x as gespiegelde roi maken
if [ "$mirror" == "ja" ]
	then
	echo "mirroring..."
	#if x<45, dan x_mirror=45+(45-x), etc.
	if [ "$x" -le "45" ]
		then
		#uitrekenen x-coordinate in andere hemisphere https://www.mkssoftware.com/docs/man1/bc.1.asp
		x_dist=$(echo "scale=1;45-$x" | bc)
		x_mirror=$(echo "scale=1;45+$x_dist" | bc)

		else
		x_dist=$(echo "scale=1;$x-45" | bc)
		x_mirror=$(echo "scale=1;45-$x_dist" | bc)
	fi
	#maken gespiegelde ROI
	fslmaths ${FSLDIR}/data/standard/MNI152_T1_2mm -mul 0 -add 1 -roi $x_mirror 1 $y 1 $z 1 0 1 ${name}_point_mirror -odt float
	fslmaths ${name}_point_mirror -kernel sphere 3 -fmean -bin ${name}_sphere_mirror -odt float
	fsladd ${name}_bilateral ${name}_sphere_mirror ${name}_sphere
	
	echo "opening fslview"
	fslview ${FSLDIR}/data/standard/MNI152_T1_2mm ${name}_bilateral -l Red-Yellow &
	
	else
	echo "opening fslview"
	fslview ${FSLDIR}/data/standard/MNI152_T1_2mm ${name}_sphere -l Red-Yellow &
fi
cd ${basedir}

