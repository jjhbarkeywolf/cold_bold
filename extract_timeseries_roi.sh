#/bin/bash
module load fsl/5.0.9-shark

#extraheert timeseries van een gespecificeerde arbitrary ROI
#kan even duren.....

#Voorbeeld:
#bash ./extract_intensity_roi.sh $roi $filelist
#bash ./extract_intensity_roi.sh /data/DIV2/radiologie-hpc/JurriaanBarkeyWolf/poepROI.nii.gz /data/DIV2/radiologie-hpc/JurriaanBarkeyWolf/filelist.txt

#$roi: geef niifile van ROI mask (absolute path)
#$filelist geef filelist met de volledige paths naar de files waar je de timeseries van wil extraheren. (absolute path)
	#bijvoorbeeld te maken d.m.v `ls -d ./data/*filtered_func_data.nii.gz`

roi=$1
filelist=$2

ROIname=`basename ${roi} .nii.gz`
mkdir -p ${ROIname}_timeseries

basedir=`pwd`
cd ${ROIname}_timeseries

#extraheren timeseries
for input in `cat ${filelist}` 
do
	basename=`basename $input .nii.gz`
	echo "extracting timeseries of ${ROIname} for ${basename}..."
	
	echo $basename > ${basename}_meants.txt
	fslmeants -i ${input} -m $roi >> ${basename}_meants.txt
done

#samenvoegen van alle timeseries textfiles
echo "combining timeseries to one .csv...."
paste -d "," *_meants.txt > ${ROIname}_timeseries.csv