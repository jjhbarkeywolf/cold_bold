#!/bin/bash



#nb threshold van 10 op lowest summed is vingerspitzengefuhl
#kijk wat het mooiste resultaat oplevert
mkdir -p mask
for input in `cat prepost.txt`
do
	filename=`basename ${input} .nii.gz`
	lowest=`fslstats ./${input} -P 10`
	fslmaths ./${input} -thr ${lowest} ./mask/${filename}_thr_lowest

done

fsladd ./mask/lowest_summed `ls ./mask/*.nii.gz`

fslmaths ./mask/lowest_summed -thr 10 -bin ./mask/lowest_summed_binned


fslview ./mask/lowest_summed ./mask/lowest_summed_binned &

fslmaths ./mask/lowest_summed_binned ./stats/brain_mask

