Repository voor allerlei simpele scripts gerelateerd aan BOLD-intensity analyses. Als eerste getest in de context van de COLD1 resting state data.
Scripts zijn 'hacky', maar zouden redelijk voor zichzelf moeten spreken.

Voel je vrij om alles aan te passen en waar dan ook voor te gebruiken.

Mij kan je echter altijd mailen voor vragen.
Contact: jjhbarkeywolf@gmail.com