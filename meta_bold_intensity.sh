#!/bin/bash
#$ -S /bin/bash
#$ -q all.q
#$ -N meta_bold_intensity
#$ -pe BWA 2
#$ -cwd
#$ -j Y
#$ -V
#$ -m be



#$2 is pp (Cold001 bijv.) $3 is pre of post
#sourcedir=$1
#pp=$2
#prepost=$3
#sourcedir is hardcoded in bold_intensity.sh
#SCRIPT DUMPT OUTPUTS IN WORKING DIRECTORY!

sourcedir=/data/DIV2/radiologie-hpc/JurriaanBarkeyWolf/COLD_BOLD/COLD1_raw/Glucose_22_graden
wd=`pwd`
prepost='pre post'
#inclusiefile als vervanging voor grep / ls combo. Bevat alleen bijv. 'Cold001' en niet het hele path
#inclusiefile is gegenereerd door #`ls -d ${sourcedir}/Cold*3DT1* | grep -o Cold[0-9][0-9][0-9]`
inclusiefile=`cat inclusiefile.txt`

for pp in ${inclusiefile} 
do
	for time in ${prepost}
	do
		qsub ${wd}/bold_intensity.sh ${sourcedir} ${pp} ${time}
	done
done
